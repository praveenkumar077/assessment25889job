package com.htc.spring5demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.htc.spring5demo.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, String>{

	
}
