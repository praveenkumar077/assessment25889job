package com.htc.spring5demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.htc.spring5demo.model.Product;
import com.htc.spring5demo.repository.ProductRepository;

@Service("productService")
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	
	@Transactional
	public boolean addProduct(Product product) {
		if(productRepository.save(product) !=null) 
			return true;
		else
			return false;
	}
	
	public Product getProduct(String productCode) {
		Optional<Product> productOpt = productRepository.findById(productCode);
		if(productOpt.isPresent()) {
			return productOpt.get();
		}
		else
			return null;
	}
	
	public Iterable<Product> getProducts() {
		return productRepository.findAll();
	}
	
	public boolean removeProduct(String productCode) {
		productRepository.deleteById(productCode);
		return true;
	}
}
