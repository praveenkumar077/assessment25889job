package com.htc.spring5demo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.htc.spring5demo.config.SpringAppConfig;
import com.htc.spring5demo.model.Product;
import com.htc.spring5demo.service.ProductService;

public class SpringDataJPA_Test {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringAppConfig.class);
		ProductService productService = (ProductService)context.getBean("productService");
		
		boolean result = productService.addProduct(new Product("p0006","Scale","stationary", 10.0, 40));
		System.out.println(result);
		System.out.println("-------------------------------------------------------------");
		System.out.println(productService.getProduct("p0006"));
		System.out.println("=============================================================");
		for(Product p : productService.getProducts()) {
			System.out.println(p);
		}
		
	}
	
}
